using System;

namespace Warcraft
{
    public class Peasant
    {
        private string name;
        private string race;
        private string faction;
        private int hit_points;
        private int armor;

        public Peasant(string name)
        {
            throw new NotImplementedException();
        }

        public string SayHello()
        {
            throw new NotImplementedException();
        }

        public string Grunt()
        {
            throw new NotImplementedException();
        }

        public string Talk()
        {
            throw new NotImplementedException();
        }

        public string TalkToPeasant(Peasant peasant)
        {
            throw new NotImplementedException();
        }

        public string TalkToPeon(Peon peon)
        {
            throw new NotImplementedException();
        }

        public string GetName()
        {
            return this.name;
        }

        public string GetRace()
        {
            return this.race;
        }

        public string GetFaction()
        {
            return this.faction;
        }

        public int GetHitPoints()
        {
            return this.hit_points;
        }

        public int SetHitPoints()
        {
            return this.hit_points;
        }

        public int GetArmor()
        {
            return this.armor;
        }
    }
}