using System;
using Xunit;
using Warcraft;

namespace warcrafttest
{
    public class PeonTest
    {
        [Fact]
        public void TestConstrutor()
        {
            Peon peon = new Peon("Thrall");
            Assert.Equal("Thrall", peon.GetName());
            Assert.Equal("Orc", peon.GetRace());
            Assert.Equal("Horde", peon.GetFaction());
            Assert.Equal(30, peon.GetHitPoints());
            Assert.Equal(0, peon.GetArmor());
        }

        [Fact]
        public void TestSayHello()
        {
            Peon peon = new Peon("Thrall");
            Assert.Equal("Je suis Thrall l'orc peon, je fais partie de la Horde et j'ai 30 points de vie.", peon.SayHello());
        }

        [Fact]
        public void TestGrunt()
        {
            Peon peon = new Peon("Thrall");
            Assert.Equal("Je suis un grand méchant Orc!", peon.Grunt());
        }

        [Fact]
        public void TestTalk()
        {
            Peon peon = new Peon("Thrall");
            Assert.Equal("Je suis Thrall et je parle orc.", peon.Talk());
        }

        [Fact]
        public void TestTalkToPeasant()
        {
            Peon peon = new Peon("Thrall");
            Peasant peasant = new Peasant("Arthas");
            Assert.Equal("Je suis Thrall l'orc peon et je parle à Arthas le paysan humain.", peon.TalkToPeasant(peasant));
        }

        public void TestTalkToPeon()
        {
            Peon thrall = new Peon("Thrall");
            Peon rehgar = new Peon("Rehgar");
            Assert.Equal("Je suis Thrall l'orc peon et je parle à Rehgar un autre orc peon.", thrall.TalkToPeon(rehgar));

            Assert.Equal("Je suis Rehgar l'orc peon et je parle à Thrall un autre orc peon.", rehgar.TalkToPeon(thrall));

            Assert.Equal("Je suis Thrall l'orc peon et je ne peux pas me parler à moi-même.", thrall.TalkToPeon(thrall));
        }
    }
}
