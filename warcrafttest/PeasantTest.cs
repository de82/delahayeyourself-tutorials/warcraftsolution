using System;
using Xunit;

using Warcraft;

namespace warcrafttest
{
    public class PeasantTest
    {
         [Fact]
        public void TestConstrutor()
        {
            Peasant peasant = new Peasant("Arthas");
            Assert.Equal("Arthas", peasant.GetName());
            Assert.Equal("Humain", peasant.GetRace());
            Assert.Equal("Alliance", peasant.GetFaction());
            Assert.Equal(30, peasant.GetHitPoints());
            Assert.Equal(0, peasant.GetArmor());
        }

        [Fact]
        public void TestSayHello()
        {
            Peasant peasant = new Peasant("Arthas");
            Assert.Equal("Je suis Arthas un paysan humain, je fais partie de l'lliance et j'ai 30 points de vie.", peasant.SayHello());
        }

        [Fact]
        public void TestGrunt()
        {
            Peasant peasant = new Peasant("Arthas");
            Assert.Equal("Je suis un grand guerrier!", peasant.Grunt());
        }

        [Fact]
        public void TestTalk()
        {
            Peasant peasant = new Peasant("Arthas");
            Assert.Equal("Je suis Arthas et je parle humain.", peasant.Talk());
        }

        [Fact]
        public void TestTalkToPeasant()
        {
            Peasant arthas = new Peasant("Arthas");
            Peasant uther = new Peasant("Uther");
            Assert.Equal("Je suis Arthas un paysan humain et je parle à Uther un autre paysan humain.", arthas.TalkToPeasant(uther));

            Assert.Equal("Je suis Uther un paysan humain et je parle à Arthas un autre paysan humain.", uther.TalkToPeasant(arthas));

            Assert.Equal("Je suis Arthas un paysan humain et je ne peux pas me parler à moi-même.", arthas.TalkToPeasant(arthas));
        }

        public void TestTalkToPeon()
        {
            Peon thrall = new Peon("Thrall");
            Peasant peasant = new Peasant("Arthas");
            Assert.Equal("Je suis Arthas un paysan humain et je parle à Thrall un peon orc.", peasant.TalkToPeon(thrall));
        }
    }
}
